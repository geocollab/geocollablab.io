---
title: "Developer Documentation"
linkTitle: "Developer Documentation"
weight: 10
---

If you want to get started with this project you can look for details in each repository in the `CONTRIBUTING.md` file.
We are not overly picky, but we have our core principles if you won't comply with them we won't accept your submissions.

A high-level overview over our repositories and their functions:

- [geocollab.gitlab.io](https://gitlab.com/geocollab/geocollab.gitlab.io): These are the sources for this website. It is
  built with Hugo and the Google Docsy Theme.
- [https://gitlab.com/geocollab/geocollabweb](https://gitlab.com/geocollab/geocollabweb): The webinterface which will
  present the main userinterface for using and controlling the server and of course the UI itself.
- [https://gitlab.com/geocollab/geocollabserver](https://gitlab.com/geocollab/geocollabserver): This is the main server.
  It consists of a PostgreSQL database and a go application. This will never render anything useful for an enduser, just
  expose information in JSON. The API is described with the openAPI 3.01 standard.

Design concepts will be maintained here, as well as more general information. The code itself will be documented with
markup and inline comments as good as reasonably possible.
