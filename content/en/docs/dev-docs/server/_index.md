---
title: "Server"
linkTitle: "Server"
weight: 10
---

## General things

Our main technologies we are:

- Language: Go - <https://golang.org/doc/>
- ORM: GORM - <https://gorm.io>
- API: We are building a JSON-REST API with a generated openAPI Specification
  - Router: <https://gin-gonic.com/> - <https://github.com/gin-gonic/gin>
  - openAPI Generator: <https://github.com/swaggo/gin-swagger>
- CLI: [Urfave V2](https://github.com/urfave/cli)
- Reading GPX Data: <https://github.com/twpayne/go-gpx>
- Authentication: 
  - SSO Server: [Casdoor](https://casdoor.org/)
  - SDK: <https://github.com/casdoor/casdoor-go-sdk>
- Authorization:
  - [Casbin](https://casbin.org/)
  - [Gin Casbin Integration](https://github.com/gin-contrib/authz)

We have an `.editorconfig` and will try to enforce the rules via our Gitlab CI. Tests are not there yet but will be an
integrational part as of our first release. 

## API

- REST
- openAPI
- JSON
- OAuth for authentication
- Multi User Support
- Import should be possible from:
  - GSAK Sqlite Databases
  - GPX Files (main format)
  - geocaching.com scrapping (like c:geo with the user who is active or with a user the admin defined)
  - In the future the official API possibly (needs official help by Groundspeak though)
- Language of the API: English --> Translations endpoint provides additional languages. 

## Database

- All data should be stored in plaintext since encryption and decryption is hard. Everyone can self host if the hoster
  is not to be trusted in the eyes of the user.
- Database migrations are required to ensure a smooth upgrade experience. Downgrades won't be supported.

Current data categories (attributes are more ideas rather then a list):

- Waypoints:
  - Public: Original Cache Coordinates & Subwaypoints
  - Private: User defined waypoints
- Logs (public): Name, Date, Text
- Cache Details:
  - Public:  Name, Terrain, Difficulty, Favorite Point Count, Cache Description, etc.
  - Private: Corrected Coordinates, Cache Note  
- Attributes (public): Icon, Name, Description
- Translations (public): Identifier, Language, Translated Text

## Security

- User authentication will be done via OAuth Tokens.
- Frontends will never save data permanently.
- Basic Auth won't be supported.
- Public data may be queried without authentication.
- Our application may never write data to the disc except for images or other documents.
- Our application may only modify its own config file at runtime to persist its configuration in the YAML format.
- We will ship our own symbols. A user may add icons in a (via config file) specified directory additionally.
- We may never call binaries in the OS. We will only use Libraries. If no library exists, we will contribute language
  bindings to the project if possible or maintain them in our own organization.

## Packaging

We will package our application in the openSUSE Build Service and provide RPMs and DEBs for Releases.
