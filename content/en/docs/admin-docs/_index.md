---
title: "Administrator Documentation"
linkTitle: "Administrator Documentation"
weight: 10
---

The most easy way to set the project up will always be Docker-Compose as this will build the project always from source.
If you are more of a "classic" release type of admin feel free to use the distribution packages we will provide after
the first stable release via the openSUSE Buildservice.
