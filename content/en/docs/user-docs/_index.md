---
title: "User Documentation"
linkTitle: "User Documentation"
weight: 10
---

This project is so far not ready for the broader audience. If you would like to become an early adaptor then please
contact [SchoolGuy](matrixfueller@gmail.com).

As an early adaptor the following things are required or desired:

- English as this is the main project communication language.
- A Gitlab account to participate in the Issues and Discussions.
- You need to join at least one of our Gitter Rooms.
- Be constructive and precise. Sloppy or too general feedback will just be ignored as this is not helpful for the
  project.

I (SchoolGuy) will happily invest time in introducing you and giving you a short tour to this project as any help the
project can get is very much appreciated.
