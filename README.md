# geocollab.gitlab.io

This website is our main information base for the project. We store user, admin and developer information which is not
code related in here.

## Technical information about this site

We are using Hugo with the Docsy Theme from Google.

## How to use Hugo and Docsy?

- [Hugo Docs](https://gohugo.io/documentation/)
- [Docsy Repo](https://github.com/google/docsy)
- [Docsy Docs](https://docsy.dev/docs/)

## Building this project

The mosic basic way to build this is:

```
hugo
```

For more options see: <https://gohugo.io/commands/hugo/>

## Running the website locally

```
hugo server
```
